package com.example.jam.searchmanagerdemo;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SearchManagerDemoActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText etdata;
    private Button btnsearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchmanagerdemo);
        etdata = (EditText)findViewById(R.id.etdata);
        btnsearch = (Button)findViewById(R.id.btncall);
        btnsearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onSearchRequested();
    }

    @Override
    public boolean onSearchRequested() {
        String text = etdata.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString("data",text);
        startSearch("Jam",false,bundle,false);
        return super.onSearchRequested();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
