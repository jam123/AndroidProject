package com.example.jam.logactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LogActivity extends Activity {
    private Button myButton;
    static final String LIFT_TAG="LogActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.v(LogActivity.LIFT_TAG,"FirstActivity ---> onCreate");
        Button myButton = (Button)findViewById(R.id.myButton);
        myButton.setText("启动第二个Activity");
        myButton.setOnClickListener(new ButtonOnClickListener());
    }
    class ButtonOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(LogActivity.this,LogActivity2.class);
            LogActivity.this.startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        Log.v(LogActivity.LIFT_TAG,"FirstActivity ---> onDestory");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.v(LogActivity.LIFT_TAG,"FirstActivity ---> onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        Log.v(LogActivity.LIFT_TAG, "FirstAcvity --->onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Log.v(LogActivity.LIFT_TAG, "FirstAcvity --->onResume");
        super.onResume();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        Log.v(LogActivity.LIFT_TAG, "FirstAcvity --->onStart");
        super.onStart();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.v(LogActivity.LIFT_TAG, "FirstAcvity --->onStop");
        super.onStop();
    }

}
