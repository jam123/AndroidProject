package com.example.jam.dialogalert;

import android.app.Activity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;

public class DialogAlert extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Builder dialog = new AlertDialog.Builder(DialogAlert.this);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle("Welcome!!!");
        dialog.setMessage("欢迎使用DialogAlert版本1.0!");
        dialog.setPositiveButton("Yes", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface a0, int a1) {
                    //可插入按下“Yes”后要处理的事情

                    }
                }
        );
            dialog.setNegativeButton("No", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //可插入按下"No"后要处理的事情
                }
            });
         dialog.create();
         dialog.show();
    }
}
