package com.example.jam.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Imagetext> mImagetextList;

    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;

        public ViewHolder(View view){
            super(view);
            imageView=(ImageView)view.findViewById(R.id.image);
            textView=(TextView)view.findViewById(R.id.text);
        }
    }

    public RecyclerViewAdapter(List<Imagetext>imagetextList){
        mImagetextList=imagetextList;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public void onBindViewHolder(ViewHolder holder, int position){
        Imagetext imagetext = mImagetextList.get(position);
        holder.imageView.setImageResource(imagetext.getImageId());
        holder.textView.setText(imagetext.getText());
    }
    public int getItemCount(){
        return mImagetextList.size();
    }
}
