package com.example.jam.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {

    private List<Imagetext> imagetextList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_layout);

        init();

        android.support.v7.widget.RecyclerView recyclerView = findViewById(R.id.recycle_view);
        //纵向列表
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //纵向列表时的分割线
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));

        //横向列表
//        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        //横向列表时候的分割线
//        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL));

//        //网格布局的列表
//        GridLayoutManager layoutManager=new GridLayoutManager ( this,3 );
//        //网格列表时候的分割线
//        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL));


        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter  adapter = new RecyclerViewAdapter(imagetextList);
        recyclerView.setAdapter(adapter);

    }

    private void init(){
        for(int i=0;i<5;i++){
            Imagetext a =new Imagetext("A",R.mipmap.ic_launcher);
            imagetextList.add(a);
            Imagetext b =new Imagetext("B",R.mipmap.ic_launcher);
            imagetextList.add(b);
            Imagetext c =new Imagetext("C",R.mipmap.ic_launcher);
            imagetextList.add(c);
            Imagetext d =new Imagetext("D",R.mipmap.ic_launcher);
            imagetextList.add(d);
            Imagetext e =new Imagetext("E",R.mipmap.ic_launcher);
            imagetextList.add(e);
            Imagetext f =new Imagetext("F",R.mipmap.ic_launcher);
            imagetextList.add(f);
        }
    }
}
