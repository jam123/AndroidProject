package com.example.jam.recyclerview;

public class Imagetext {
    private String text;
    private int imageId;

    public  Imagetext(String text,int imageId){
        this.text=text;
        this.imageId=imageId;
    }

    public String getText(){
        return text;
    }
    public int getImageId(){
        return imageId;
    }
}
