package com.example.jam.intentreceiverinjava;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class IntentReceiverInJavaActivity extends AppCompatActivity {
    private Button registerButton = null;
    private Button unregisterButton = null;
    private SMSReceiverActivity smsReceiver = null;

    private static final String SMS_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_receiverinjava);
        registerButton = (Button)findViewById(R.id.register);
        registerButton.setOnClickListener(new RegisterReceiverListener());
        unregisterButton = (Button)findViewById(R.id.unregister);
        unregisterButton.setOnClickListener(new UnRegisterReceiverListener());
    }

    class RegisterReceiverListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            smsReceiver = new SMSReceiverActivity();
            IntentFilter filter = new IntentFilter();
            filter.addAction(SMS_ACTION);
            IntentReceiverInJavaActivity.this.registerReceiver(smsReceiver,filter);
        }
    }

    class UnRegisterReceiverListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            IntentReceiverInJavaActivity.this.unregisterReceiver(smsReceiver);
        }
    }
}

