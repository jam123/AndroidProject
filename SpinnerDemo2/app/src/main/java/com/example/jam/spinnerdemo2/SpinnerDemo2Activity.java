package com.example.jam.spinnerdemo2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SpinnerDemo2Activity extends AppCompatActivity {

    private static final String[] countriesStr = { "广州", "上海", "北京", "深圳" };
    private TextView myTextView;
    private EditText myEditText;
    private Button myButton_add;
    private Button myButton_remove;
    private Spinner mySpinner;
    private ArrayAdapter adapter;
    private List allCountries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allCountries = new ArrayList();
        for(int i=0;i<countriesStr.length;i++)
        {
            allCountries.add(countriesStr[i]);
        }

        adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,allCountries);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        myTextView = (TextView)findViewById(R.id.myTextView);
        myEditText = (EditText)findViewById(R.id.myEditText);
        myButton_add = (Button)findViewById(R.id.myButton_add);
        myButton_remove = (Button)findViewById(R.id.myButton_remove);
        mySpinner = (Spinner)findViewById(R.id.mySpinner);

        mySpinner.setAdapter(adapter);
        myButton_add.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String newCountry = myEditText.getText().toString();

                for(int i=0;i<adapter.getCount();i++)
                {
                    if(newCountry.equals(adapter.getItem(i)))
                    {
                        return;
                    }
                }
                if(!newCountry.equals(""))
                {
                    adapter.add(newCountry);
                    int position = adapter.getPosition(newCountry);
                    //将Spinner选在新增的位置上
                    mySpinner.setSelection(position);
                    //清空输入的文字
                    myEditText.setText("");
                }
            }
        });
        myButton_remove.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(mySpinner.getSelectedItem()!=null)
                {
                    adapter.remove(mySpinner.getSelectedItem().toString());
                    myEditText.setText("");
                    if(adapter.getCount()==0)
                    {
                        myTextView.setText("");
                    }
                }
            }
        });
        mySpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                myTextView.setText(parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.spinner_demo2,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
