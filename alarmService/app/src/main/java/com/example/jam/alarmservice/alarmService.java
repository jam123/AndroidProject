package com.example.jam.alarmservice;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class alarmService extends AppCompatActivity implements View.OnClickListener{
    private static  alarmService appRef = null;
    private Button b_call_service,b_exit_service;
    boolean k = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appRef = this;
        setContentView(R.layout.activity_alarmservice);
        b_call_service = (Button) findViewById(R.id.call_alarm_service);
        b_call_service.setOnClickListener(this);
        b_exit_service = (Button)findViewById(R.id.exit);
        b_exit_service.setOnClickListener(this);
    }

    public static alarmService getApp(){
        return appRef;
    }

    public void btEvent(String data){
        setTitle(data);
    }

    @Override
    public void onClick(View v) {
        if(v == b_call_service) {
            setTitle("等待5秒!!!!!");
            Intent intent = new Intent(alarmService.this, AlarmReceiver.class);
            PendingIntent p_intent = PendingIntent.getBroadcast(alarmService.this, 0, intent, 0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.SECOND, 5);
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), p_intent);
        }

        if(v == b_exit_service){
            Intent intent = new Intent(alarmService.this, AlarmReceiver.class);
            PendingIntent p_intent = PendingIntent.getBroadcast(alarmService.this, 0, intent, 0);
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            am.cancel(p_intent);
            finish();
        }
    }
}
