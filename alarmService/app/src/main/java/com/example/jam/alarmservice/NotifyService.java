package com.example.jam.alarmservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class NotifyService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        alarmService app = alarmService.getApp();
        app.btEvent("from NotifyService");
        Toast.makeText(this,"闹钟响起",Toast.LENGTH_LONG).show();
    }
}
