package com.example.jam.autocompletetextviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;

public class AutoCompleteTextViewDemoActivity extends AppCompatActivity {
    private static final String[] autostr = new String[] {"Italy","IT","item","its","itself"
            ,"abc","abcd","abcde"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autocompletemain);

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,autostr);
        AutoCompleteTextView myAutoCompleteTextView =(AutoCompleteTextView) findViewById(R.id.myAutoCompleteTextView);
        myAutoCompleteTextView.setAdapter(adapter);

        MultiAutoCompleteTextView myMultiAutoCompleteTextView = (MultiAutoCompleteTextView)
                findViewById(R.id.myMultiAutoCompleteTextView);
        myMultiAutoCompleteTextView.setAdapter(adapter);
        myMultiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
    }
}
