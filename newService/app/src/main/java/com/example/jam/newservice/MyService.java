package com.example.jam.newservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


public class MyService extends Service {
    public IBinder onBind(Intent intent){
        Log.e("My Service","My Service.onBind()");
        Toast.makeText(this,"onBind()",Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    public void onCreate() {
        Log.e("My Service","My Service.onCreate()");
        Toast.makeText(this,"onCreate()",Toast.LENGTH_SHORT).show();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("My Service","My Service.onStartCommand()");
        Toast.makeText(this,"onStartCommand()",Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.e("My Service","My Service.onDestroy()");
        Toast.makeText(this,"onDestroy()",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("My Service","My Service.onUnbind()");
        Toast.makeText(this,"onUnbind()",Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }
}
