package com.example.jam.newservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btn_StartService;
    private Button btn_StopService;
    private Button btn_BindService;
    private Button btn_UnbindService;

    private ServiceConnection mServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViews();
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };
    }

    public void setupViews(){
        btn_StartService = (Button)findViewById(R.id.button1);
        btn_StopService = (Button)findViewById(R.id.button2);
        btn_BindService = (Button)findViewById(R.id.button3);
        btn_UnbindService = (Button)findViewById(R.id.button4);

        btn_StartService.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyService.class);
                startService(intent);
            }
        });
        btn_StopService.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyService.class);
                stopService(intent);
            }
        });
        btn_BindService.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyService.class);
                bindService(intent,mServiceConnection,BIND_AUTO_CREATE);
            }
        });
        btn_UnbindService.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyService.class);
                unbindService(mServiceConnection);
            }
        });
    }

    @Override
    protected void onDestroy() {
        Log.e("MainActivity","Activity.onDestory()");
        Toast.makeText(this,"45Activity.onDestory()",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
