package com.example.jam.fragement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentDemoActivity extends AppCompatActivity {

    public static String[] array ={"A","B","C","D","E","F","G","H"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentdemo);
    }

    public static class TitlesFragment extends ListFragment{

        boolean mDualPane;
        int mCurCheckPosition = 0;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            System.out.println("Fragment-->onCreate");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            System.out.println("Fragment-->onCreatView");
            return super.onCreateView(inflater, container, savedInstanceState);
        }

        @Override
        public void onPause() {
            super.onPause();
            System.out.println("Fragment-->onPause");
        }

        @Override
        public void onStop() {
            super.onStop();
            System.out.println("Fragment-->onStop");
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            System.out.println("Fragment-->onAttach");
        }

        @Override
        public void onStart() {
            super.onStart();
            System.out.println("Fragment-->onStart");
        }

        @Override
        public void onResume() {
            super.onResume();
            System.out.println("Fragment-->onResume");
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            System.out.println("Fragment-->onDestroy");
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            System.out.println("Fragment-->onActivityCreated");
            setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, array));

            View detailsFrame = getActivity().findViewById(R.id.details);

            mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

            if (savedInstanceState != null) {
                mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
            }

            if (mDualPane) {
                getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                showDetails(mCurCheckPosition);
            }
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt("curChoice",mCurCheckPosition);
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            showDetails(position);
        }

        void showDetails(int index){
            mCurCheckPosition = index;
            if(mDualPane){
                getListView().setItemChecked(index,true);
                DetailsFragment details = (DetailsFragment) getFragmentManager().findFragmentById(R.id.details);
                if(details == null || details.getShownIndex() != index){
                    details = DetailsFragment.newInstance(mCurCheckPosition);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();

                    ft.replace(R.id.details,details);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                }
            }else{
                new AlertDialog.Builder(getActivity())
                        .setTitle(android.R.string.dialog_alert_title)
                        .setMessage(array[index])
                        .setPositiveButton(android.R.string.ok,null).show();
            }
        }
    }

    public static class DetailsFragment extends Fragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public static DetailsFragment newInstance(int index){
            DetailsFragment details = new DetailsFragment();
            Bundle args = new Bundle();
            args.putInt("index",index);
            details.setArguments(args);
            return details;
        }

        public int getShownIndex(){
            return getArguments().getInt("index",0);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            if(container == null)
                return null;
            ScrollView scroller = new ScrollView(getActivity());
            TextView text = new TextView(getActivity());

            int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,4,getActivity().getResources().getDisplayMetrics());
            text.setPadding(padding,padding,padding,padding);
            scroller.addView(text);
            text.setText(array[getShownIndex()]);
            return scroller;
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

            super.onCreateOptionsMenu(menu, inflater);
            menu.add("Menu 1a").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            menu.add("Menu 1b").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            Toast.makeText(getActivity(),"index is"+getShownIndex()+"&& menu text is "+ item.getTitle(),Toast.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }
    }
}

