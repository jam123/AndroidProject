package com.example.jam.newdialer2;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewDialer2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdialer2);

        final EditText phoneNum = (EditText)findViewById(R.id.phoneNum);
        final Button button = (Button)findViewById(R.id.Button01);
        button.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                String call = phoneNum.getText().toString();
                if(PhoneNumberUtils.isGlobalPhoneNumber(call)){
                    //Android6.0以上需要动态申请权限
                    ActivityCompat.requestPermissions(NewDialer2Activity.this, new String[]{Manifest.permission.CALL_PHONE},1);
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+call));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(NewDialer2Activity.this,R.string.notify_incorrect_phonenum,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
