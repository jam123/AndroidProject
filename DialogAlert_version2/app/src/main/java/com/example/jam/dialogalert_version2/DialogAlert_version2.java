package com.example.jam.dialogalert_version2;

import android.app.Activity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class DialogAlert_version2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Builder dialog = new AlertDialog.Builder(DialogAlert_version2.this);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle("Welcome!!!");
        dialog.setMessage("你使用的是DialogAlert版本2.0");
        dialog.setPositiveButton("Yes", new OnClickListener() {
            @Override
            public void onClick(DialogInterface a0, int a1) {
                //可插入按下“Yes”后要处理的事情
            }
        });
        dialog.setNegativeButton("No", new OnClickListener() {
            @Override
            public void onClick(DialogInterface a0, int a1) {
                //可插入按下"No"后要处理的事情
            }
        });
        dialog.create();
        dialog.show();
    }
}
