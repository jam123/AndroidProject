package com.example.jam.simpleplay;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class MusicService extends Service {
    private MediaPlayer mPlayer;

    @Override
    public void onCreate() {
        mPlayer = MediaPlayer.create(getApplicationContext(),R.raw.ring);
        mPlayer.setLooping(true);
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Toast.makeText(MusicService.this,"MusicSevice onStart()",Toast.LENGTH_SHORT).show();
        mPlayer.start();
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(MusicService.this,"MusicSevice onDestroy()",Toast.LENGTH_SHORT).show();
        mPlayer.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
