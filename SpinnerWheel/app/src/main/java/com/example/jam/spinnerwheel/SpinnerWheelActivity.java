package com.example.jam.spinnerwheel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class SpinnerWheelActivity extends AppCompatActivity {
    private ListView ListView01;
    private TextView TextView01;
    String[] s1 =
            { "", "", "Ipad2", "Iphone4", "I9000", "Itouch", "NANO", "P1000","Android", "Nokia", "", "" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ListView01 = (ListView) findViewById(R.id.ListView01);
        TextView01 = (TextView) findViewById(R.id.TextView01);

        ArrayAdapter<String> list1 = new ArrayAdapter<String>(this,R.layout.file_row,s1);
        ListView01.setAdapter(list1);
        ListView01.setCacheColorHint(00000000);
        ListView01.setFastScrollEnabled(true);
        ListView01.setFadingEdgeLength(100);
        ListView01.setOnScrollListener(new ListView.OnScrollListener(){
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                TextView01.setText(s1[firstVisibleItem + 2]);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });

        ListView01.setOnItemClickListener(new ListView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int id, long arg3) {
                ListView01.setSelectionFromTop(id - 2, 0);
                TextView01.setText(s1[id]);
            }
        });

        ListView01.setOnItemSelectedListener(new ListView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int id, long arg3) {
                TextView01.setText(s1[id]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.spinner_wheel,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
