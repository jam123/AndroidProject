package com.example.jam.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewActivity extends AppCompatActivity {

    private String[] weekStrings={"星期日","星期一","星期二","星期三","星期四",
            "星期五","星期六"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                ListViewActivity.this,android.R.layout.simple_list_item_1,weekStrings
        );
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener( new AdapterView.OnItemClickListener(){
             @Override
             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 ListViewActivity.this.setTitle(((TextView) view).getText());
             }
        }
        );
    }
}
